package ar.sfe.repository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import ar.sfe.model.Person;
import ar.sfe.model.Sex;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonaRepositoryTest {

	@Autowired
	private PersonRepository personaRepository;

	private Person createPerson(Long id, String name, String surname, Integer age, Sex sex) {
		Person aPerson = new Person();
		aPerson.setId(id);
		aPerson.setName(name);
		aPerson.setSurname(surname);
		aPerson.setAge(age);
		aPerson.setSex(sex);
		return aPerson;
	}

	@Test
	public void testFindByIdWithoutData() {
		// No hay personas, cualquier Id da error de persona no existente
		// when: delete all data
		personaRepository.deleteAll();
		// then: query any
		Long id = (new Random(13)).nextLong();
		Optional<Person> result = personaRepository.findById(id);
		// expect: empty result
		assertFalse(result.isPresent());
	}

	@Test
	public void testFindByIdWithData() {
		// Agregmos 1 persona, al consultar debe existir
		// when: add a person
		Long id = 2L;
		Person persona = createPerson(id, "juancito", "Perez and company", 32, Sex.MALE);
		personaRepository.save(persona);
		// then: search these Person
		Optional<Person> result = personaRepository.findById(id);
		// expect: result is present and is the same added
		assertTrue(result.isPresent());
		assertEquals(persona, result.get());
	}
}
