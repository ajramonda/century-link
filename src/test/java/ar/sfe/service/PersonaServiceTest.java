package ar.sfe.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import ar.sfe.model.Person;
import ar.sfe.model.Sex;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonaServiceTest {

	@Autowired
	private PersonService personaService;

	@Before
	public void clearData() {
		((PersonServiceImpl)personaService).clearRepo();
	}
	
	private Person createPerson(Long id, String name, String surname, Integer age, Sex sex) {
		Person aPerson = new Person();
		aPerson.setId(id);
		aPerson.setName(name);
		aPerson.setSurname(surname);
		aPerson.setAge(age);
		aPerson.setSex(sex);
		return aPerson;
	}

	@Test(expected = DuplicatePersonIdException.class)
	public void testSaveSamePerson() throws DuplicatePersonIdException {
		// when: add a person
		Person person = createPerson(1L, "juancito", "Perez and company", 32, Sex.MALE);
		personaService.save(person);
		// then: save same person
		personaService.save(person);
		// expect: DuplicatePersonIdException
	}
	
	@Test(expected = PersonNonExistentException.class)
	public void testUpdateNonExistentPerson() throws PersonNonExistentException {
		// when: create a person
		Person person = createPerson(2L, "carlos", "Menem", 32, Sex.MALE);
		// then: update non-saved person (non-existent)
		personaService.update(person);
		// expect: PersonNonExistentException
	}
}
