package ar.sfe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestPerson {

	public static void main(String[] args) {
		SpringApplication.run(RestPerson.class, args);
	}

}
