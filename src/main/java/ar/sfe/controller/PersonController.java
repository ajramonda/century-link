package ar.sfe.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.sfe.model.Person;
import ar.sfe.service.DuplicatePersonIdException;
import ar.sfe.service.PersonNonExistentException;
import ar.sfe.service.PersonService;

@RestController
@RequestMapping("/persons")
public class PersonController {

	@Autowired
	private PersonService personService;

	@GetMapping
	public Iterable<Person> get() {
		return personService.findAll();
	}

	@PostMapping
	public Person post(@Valid @RequestBody Person aPerson) throws DuplicatePersonIdException {
		return personService.save(aPerson);
	}

	@PutMapping
	public Person put(@Valid @RequestBody Person anExistentPerson) throws PersonNonExistentException {
		return personService.update(anExistentPerson);
	}

	@DeleteMapping("/{id}")
	public String delete(@PathVariable String id) throws PersonNonExistentException {
		Long personId = Long.valueOf(id);
		boolean success = personService.deleteById(personId);
		return "{ \"status\":\"" + (success ? "success" : "failure") + "\"}";
	}
}
