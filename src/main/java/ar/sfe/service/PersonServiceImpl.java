package ar.sfe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.sfe.model.Person;
import ar.sfe.repository.PersonRepository;

@Service("personaService")
public class PersonServiceImpl implements PersonService {

	@Autowired
	private PersonRepository personRepository;

	@Override
	public Iterable<Person> findAll() {
		return personRepository.findAll();
	}

	@Override
	public Person save(Person aPerson) throws DuplicatePersonIdException {
		if (personRepository.existsById(aPerson.getId())) {
			throw new DuplicatePersonIdException(aPerson.getId());
		}
		return personRepository.save(aPerson);
	}

	@Override
	public Person update(Person anExistentPerson) throws PersonNonExistentException {
		if (!personRepository.existsById(anExistentPerson.getId())) {
			throw new PersonNonExistentException(anExistentPerson.getId());
		}
		return personRepository.save(anExistentPerson);
	}

	@Override
	public boolean deleteById(Long personId) throws PersonNonExistentException {
		if (!personRepository.existsById(personId)) {
			throw new PersonNonExistentException(personId);
		}
		personRepository.deleteById(personId);
		if (personRepository.existsById(personId)) {
			return false;
		} else {
			return true;
		}
	}
	
	//only to tests
	void clearRepo() {
		personRepository.deleteAll();
	}

}
