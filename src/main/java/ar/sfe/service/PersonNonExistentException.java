package ar.sfe.service;

public class PersonNonExistentException extends Exception {

	public PersonNonExistentException(Long id) {
		super("Non existent person: " + id);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5088823314208812088L;

}
