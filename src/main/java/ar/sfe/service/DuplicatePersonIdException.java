package ar.sfe.service;

public class DuplicatePersonIdException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2106583590611449123L;

	public DuplicatePersonIdException(Long id) {
		super("Duplicated person id: " + id);
	}

}
