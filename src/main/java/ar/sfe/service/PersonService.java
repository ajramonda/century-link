package ar.sfe.service;

import ar.sfe.model.Person;

public interface PersonService {

	/**
	 * Query for all persons.
	 * 
	 * @return An {@link Iterable} of {@link Person}
	 */
	public Iterable<Person> findAll();

	/**
	 * Saves a {@link Person}.
	 * 
	 * @param aPerson {@link Person} to save
	 * @return Saved {@link Person}
	 * @throws DuplicatePersonIdException
	 */
	public Person save(Person aPerson) throws DuplicatePersonIdException;

	/**
	 * Updates an existent {@link Person}.
	 * 
	 * @param anExistentPerson {@link Person} to update
	 * @return Updated {@link Person}
	 * @throws PersonNonExistentException If {@link Person} does not exists on
	 *                                    repository
	 */
	public Person update(Person anExistentPerson) throws PersonNonExistentException;

	/**
	 * Delete an existent {@link Person}.
	 * 
	 * @param personId {@link Person} identification
	 * @return true if {@link Person} is deleted, false anywhere.
	 * @throws PersonNonExistentException If {@link Person} does not exists on
	 *                                    repository
	 */
	public boolean deleteById(Long personId) throws PersonNonExistentException;
}
