package ar.sfe.model;

public enum Sex {
	FEMALE, MALE, OTHER;
}
