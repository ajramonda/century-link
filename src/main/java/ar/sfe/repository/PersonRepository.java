package ar.sfe.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ar.sfe.model.Person;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {

}
